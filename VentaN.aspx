﻿<%@ Page Title="CLIENTE" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="ClienteN.aspx.cs" Inherits="ClienteN" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h1 class="text-center">FORMULARIO PARA VENTA</h1>
    <form class="" id="frm_nuevo_tipo" action=" clientes/guardar" method="post">
    <div class="row">
      <div class="col-md-6">
          <label for="">Descripcion: <span class="obligatorio">(obligatorio)</span> </label>
          <br>
          <input type="text"
          placeholder="Ingrese la descripcion"
          required
          class="form-control"
          name="descipcion_ven" value="" id="descipcion_ven">
      </div>
      <div class="col-md-6">
        <label for="">Fecha:<span class="obligatorio">(obligatorio)</span> </label>
        <br>
        <input type="text"
        placeholder="Ingrese la fecha"
        required
        class="form-control"
        name="fecha_ven" value="" id="fecha_ven">
      </div>
        
    </div>
        
    <br>
    
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="/TipoL"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
   </form>
    <script type="text/javascript">
  $("#frm_nuevo_tipo").validate({
    rules:{
        descripcion_ven: {
            required: true,
            minlength: 5,
            maxlength: 250,
            letras: true
        },
       fecha_ven: {
            required: true,
            minlength: 3,
            maxlength: 10,
            
        }
    },
    messages: {
        
      descripcion_ven: {
          required: "Por favor ingresa tu direccion",
          minlength: "La direccion debe tener al menos 3 caracteres",
          maxlength: "Direccion incorrecto"
      },
      
      fecha_ven: {
          required: "Por favor Ingrese su edad",
          minlength: "fecha incorrecto, ingrese al menos 1 digitos",
          maxlength: "fecha icorrecta, ingrese hasta 10 digitos",
          
      }
    }
  });
</script>

</asp:Content>
