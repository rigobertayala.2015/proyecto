﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(artesania.Startup))]
namespace artesania
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
