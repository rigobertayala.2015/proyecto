﻿<%@ Page Title="TIPOS" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="TipoN.aspx.cs" Inherits="TipoN" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h1 class="text-center">FORMULARIO PARA TIPOS DE PRODUCTOS</h1>
    <form class="" id="frm_nuevo_producto" action="Producto/" method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Nombre: <span class="obligatorio">(obligatorio)</span> </label>
          <br>
          <input type="text"
          placeholder="Ingrese nombre del producto"
          required
          class="form-control"
          name="nombre_pro" value="" id="nombre_pro">
      </div>
      <div class="col-md-4">
        <label for="">Precio:<span class="obligatorio">(obligatorio)</span> </label>
        <br>
        <input type="text"
        placeholder="Ingrese precio"
        required
        class="form-control"
        name="precio_pro" value="" id="precio_pro">
      </div>
        <div class="col-md-4">
        <label for="">Stock:<span class="obligatorio">(obligatorio)</span> </label>
        <br>
        <input type="text"
        placeholder="Ingrese cantidad de stock"
        required
        class="form-control"
        name="stock_pro" value="" id="stock_pro">
      </div>
    </div>
    <br>
    
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="/ProductoL"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
   </form>
    <script type="text/javascript">
  $("#frm_nuevo_tipo").validate({
    rules:{
        nombre_pro:{
          required:true,
          minlength:3,
          maxlength:100,
          letras:true
        },
        precio_pro:{
          required:true,
          
        },
        stock_pro: {
            required: true,
            
        }
    },
    messages:{
      nombre_pro:{
        required:"Por favor ingrese su Nombre",
        minlength:"El nombre debe tener al menos 3 caracteres",
        maxlength:"Nombre incorrecto"
      },
      precio_pro:{
        required:"Por favor ingresa el precio",
        minlength:"El precio debe ser numerico",
        maxlength:"tamaño incorrecto"
      },
      stock_pro: {
          required: "Por favor ingresa su tamaño",
          minlength: "El stock debe ser numerico",
          maxlength: "stock incorrecto"
      }
    }
  });
</script>

</asp:Content>
