﻿<%@ Page Title="TIPOS" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="TipoN.aspx.cs" Inherits="TipoN" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h1 class="text-center">FORMULARIO PARA TIPOS DE PRODUCTOS</h1>
    <form class="" id="frm_nuevo_tipo" action=" clientes/guardar" method="post">
    <div class="row">
      <div class="col-md-6">
          <label for="">Nombre: <span class="obligatorio">(obligatorio)</span> </label>
          <br>
          <input type="text"
          placeholder="Ingrese nombre"
          required
          class="form-control"
          name="nombre_tip" value="" id="nombre_tip">
      </div>
      <div class="col-md-6">
        <label for="">Tamaño:<span class="obligatorio">(obligatorio)</span> </label>
        <br>
        <input type="text"
        placeholder="Ingrese tamaño"
        required
        class="form-control"
        name="tamaño_tip" value="" id="tamaño_tip">
      </div>
    </div>
    <br>
    
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="/TipoL"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
   </form>
    <script type="text/javascript">
  $("#frm_nuevo_tipo").validate({
    rules:{
        nombre_tip:{
          required:true,
          minlength:3,
          maxlength:100,
          letras:true
        },
        tamaño_tip:{
          required:true,
          minlength:3,
          maxlength:50,
          letras:true
        }
    },
    messages:{
      nombre_tip:{
        required:"Por favor ingrese su Nombre",
        minlength:"El nombre debe tener al menos 3 caracteres",
        maxlength:"Nombre incorrecto"
      },
      tamaño_tip:{
        required:"Por favor ingresa su tamaño",
        minlength:"El tamaño debe tener al menos 3 caracteres",
        maxlength:"tamaño incorrecto"
      }
    }
  });
</script>

</asp:Content>
