﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

   <center>
        <div class="jumbotron">
        <h1>TIENDA DE ROPA "MUNAY"</h1>
        <p class="lead">"Hermosa como tú"</p>
        
    </div>
    <div class="row" >
  <div class="col-xs-6 col-md-12">
    <h1 class="text-center" style="color: SteelBlue"  > INFORMACION</h1>
  </div>
  </div>
    </center>
    
  <div class="row">
  <div class="col-md-6">
        <center><img src="img/fot1.png" width="90%" height="250px"  alt="..."></center>
  </div>
  <div class="col-md-6" >
    <h2 class="text-center" style="color: SteelBlue" >¿Quienes somos?</h2>
    <p class="text-justify">La cultura indígena es identificada solo por su vestimenta y accesorios. Su cultura
                se ha convertido en un objeto estético y político para el país, reconociéndolos en
                escenarios ya establecidos por la sociedad. Estos escenarios condicionan a la cultura
                indígena a permanecer estáticos en esos lugares y no movilizarse hacia otros. Al momento
                de movilizarse son desplazados de su cuerpo y fisionomía y reemplazados para cumplir
                con las reglas de la sociedad ecuatoriana. </p>
  </div>
</div>
    <div class="row" >
  <div class="col-xs-6 col-md-12">
    <h1 class="text-center" style="color: SteelBlue"> SERVICIOS</h1>
  </div>
  </div>
    <div class="row">
<div class=" col-md-4">
  <div class="thumbnail">
    <img src="img/fot.png" width="100%" height="200px"  alt="...">
    <div class="caption">
      <br>
      <h3>EL MEJOR SERVICIOS</h3>
      <p class="text-justify">
            Un emprendimiento impulsado por una mujer indigena, ha permitido que
           este grupo de emprendedoras unan sus conocimientos y habilidades para
           elaborar todo tipo de bisutería con piedras y mullos propios del Ecuador.
      </p>
    </div>
  </div>
</div>
<div class=" col-md-4">
  <div class="thumbnail">
    <img src="img/fot2.png" width="100%" height="200px"  alt="...">
    <div class="caption">
      <br>
      <h3>REALIZAMOS DISEÑOS PERSONALIZADOS</h3>
      <p class="text-justify">
        En Cotopaxi, un grupo de mujeres se unieron para elaborar diseños de unicos 
          combinados de naturaleza haciendo referencia a los colores del Tahuantinsuyo.
    </p>
    </div>
  </div>
</div>
<div class=" col-md-4">
  <div class="thumbnail">
    <img src="img/fot3.png" width="100%" height="200px"  alt="..." >
    <div class="caption">
      <br>
      <h3>NUESTRO UBICACION</h3>
      <p class="text-justify">
        Ubicanos frente a las canchas de la Estaciòn, guiese por el nombre del rotulo como "MUNAY beautiful"
      </p>
    </div>
  </div>
</div>
</div>
</asp:Content>
