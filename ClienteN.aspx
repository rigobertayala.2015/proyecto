﻿<%@ Page Title="CLIENTE" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="ClienteN.aspx.cs" Inherits="ClienteN" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h1 class="text-center">FORMULARIO PARA CLIENTE</h1>
    <form class="" id="frm_nuevo_tipo" action=" clientes/guardar" method="post">
    <div class="row">
      <div class="col-md-4">
          <label for="">Apellido: <span class="obligatorio">(obligatorio)</span> </label>
          <br>
          <input type="text"
          placeholder="Ingrese su apellido"
          required
          class="form-control"
          name="apellido_cli" value="" id="apellido_cli">
      </div>
      <div class="col-md-4">
        <label for="">Nombre:<span class="obligatorio">(obligatorio)</span> </label>
        <br>
        <input type="text"
        placeholder="Ingrese su nombre"
        required
        class="form-control"
        name="nombre_cli" value="" id="nombre_cli">
      </div>
        <div class="col-md-4">
        <label for="">Edad:<span class="obligatorio">(obligatorio)</span> </label>
        <br>
        <input type="number"
        placeholder="Ingrese su edad"
        required
        class="form-control"
        name="edad_cli" value="" id="edad_cli">
      </div>
    </div>
        <div class="row">
      <div class="col-md-4">
          <label for="">Direccion: <span class="obligatorio">(obligatorio)</span> </label>
          <br>
          <input type="text"
          placeholder="Ingrese la direccion "
          required
          class="form-control"
          name="direccion_cli" value="" id="direccion_cli">
      </div>
       <div class="col-md-4">
        <label for="">Teléfono:<span class="obligatorio">(obligatorio)</span> </label>
        <br>
        <input type="number"
        placeholder="Ingrese el telefono"
        required
        class="form-control"
        name="telefono_cli" value="" id="telefono_cli">
      </div>
      <div class="col-md-4">
        <label for="">Email: <span class="obligatorio">(obligatorio)</span> </label>
        <br>
        <input type="email"
        placeholder="Ingrese su correo electronico"
        required
        class="form-control"
        name="email_cli" value="" id="email_cli">
      </div>
        
    </div>
    <br>
    
    <br>
    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button"
            class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="/TipoL"
              class="btn btn-danger">
              Cancelar
            </a>
        </div>
    </div>
   </form>
    <script type="text/javascript">
  $("#frm_nuevo_tipo").validate({
    rules:{
        apellido_cli: {
            required: true,
            minlength: 3,
            maxlength: 100,
            letras: true
        },
        nombre_cli: {
          required:true,
          minlength:3,
          maxlength:100,
          letras:true
        },
        direccion_cli: {
            required: true,
            minlength: 5,
            maxlength: 250,
            letras: true
        },
        telefono_cli: {
            required: true,
            minlength: 10,
            maxlength: 10,
            digits: true
        },
        edad_cli: {
            required: true,
            minlength: 1,
            maxlength: 3,
            digits: true
        },
    },
    messages: {
        apellido_tip: {
            required: "Por favor ingrese su Nombre",
            minlength: "El apellido debe tener al menos 3 caracteres",
            maxlength: "Apellido incorrecto"
        },
      nombre_tip:{
        required:"Por favor ingrese su Nombre",
        minlength:"El nombre debe tener al menos 3 caracteres",
        maxlength:"Nombre incorrecto"
      },
      direccion_cli: {
          required: "Por favor ingresa tu direccion",
          minlength: "La direccion debe tener al menos 3 caracteres",
          maxlength: "Direccion incorrecto"
      },
      telefono_cli: {
          required: "Por favor Ingrese el numero de telefono",
          minlength: "Nro. de telefono incorrecta, ingrese 10 digitos",
          maxlength: "Nro. de telefono incorrecta, ingrese 10 digitos",
          number: "Este campo solo acepta numeros"
      },
      edad_cli: {
          required: "Por favor Ingrese su edad",
          minlength: "edad incorrecto, ingrese al menos 1 digitos",
          maxlength: "edad icorrecta, ingrese hasta 3 digitos",
          number: "Este campo solo acepta numeros"
      }
    }
  });
</script>

</asp:Content>
